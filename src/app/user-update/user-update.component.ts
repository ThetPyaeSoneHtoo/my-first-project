import { Component, OnInit, Inject } from '@angular/core';
import { User } from '../model/user';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { UserService } from '../service/user.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-user-update',
  templateUrl: './user-update.component.html',
  styleUrls: ['./user-update.component.css']
})
export class UserUpdateComponent implements OnInit {

userUpdateForm;
userDateInit;

  constructor(private _snackBar: MatSnackBar, public dialogRef: MatDialogRef<UserUpdateComponent>, @Inject(MAT_DIALOG_DATA) public data: User, private userService: UserService, private router: Router) { }

  ngOnInit() {
    this.userUpdateForm=new FormGroup({
      name: new FormControl(),
      email: new FormControl('', [
        Validators.required,
        Validators.email,
      ]),
      address: new FormControl(),
      phone: new FormControl('', [
        Validators.required,
        Validators.pattern(/^-?(0|[1-9]\d*)?$/)
      ])
    });
  }

  updateUserInfo() {
    if(this.userUpdateForm.invalid){
      return;
    }else{
      this.userService.update(this.data).subscribe(result => this.gotoUserList());
    }
  }

  closeDialog(): void {
    this.dialogRef.close();
  }

  gotoUserList() {
    this.dialogRef.close();
    this._snackBar.open("Your Infomation is updated successfuly", "", {
      duration: 3000, verticalPosition: 'top',
      horizontalPosition: 'end',
    });
  }

}
