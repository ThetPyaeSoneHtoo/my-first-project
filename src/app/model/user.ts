export class User {
    id: string;
    name: string;
    password:string;
    confirmpassword:string;
    email: string;
    address:string;
    phoneno:string;
}
