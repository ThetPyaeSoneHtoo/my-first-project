import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../service/user.service';
import { User } from '../model/user';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';


@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.css']
})
export class UserFormComponent {

  user: User;
  newUserForm;

  constructor(private route: ActivatedRoute, private router: Router, private userService: UserService, private formBuilder: FormBuilder) {
    this.user = new User();
  }

  ngOnInit() {
    this.newUserForm = new FormGroup({
      name: new FormControl(),
      password: new FormControl('', [
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(12)
      ]),
      confirmpassword: new FormControl(),
      email: new FormControl('', [
        Validators.required,
        Validators.email,
      ]),
      address: new FormControl(),
      phone: new FormControl('', [
        Validators.required,
        Validators.pattern(/^-?(0|[1-9]\d*)?$/)
      ])
    });
  }

  saveNewUser() {
    if(this.newUserForm.valid){
      this.userService.save(this.user).subscribe(result => this.gotoUserList());
    }else{
      return;
    }
  }

  gotoUserList() {
    this.router.navigate(['/users']);
  }
}
