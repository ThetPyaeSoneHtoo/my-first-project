import { Component, OnInit, ViewChild } from '@angular/core';
import { User } from '../model/user';
import { UserService } from '../service/user.service';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { UserUpdateComponent } from '../user-update/user-update.component';
import { MatPaginator } from '@angular/material/paginator';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {
  users: User[];
  displayedColumns: string[] = ['id', 'name', 'email' ,'address','phoneno', 'edit', 'delete'];
  dataSource=new MatTableDataSource();

  constructor(private _snackBar: MatSnackBar, private userService: UserService, public dialog: MatDialog, private router: Router) {

  }

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  userInfo;

  async ngOnInit() {
    let response = await this.userService.findAll();
    if(response['status']=='SUCCESS'){
      this.users = response["data"];
      this.dataSource = new MatTableDataSource(this.users);
    }else if(response['messageCode']=='DB_PROCESS_ERROR'){
      this._snackBar.open(response["message"], "", {
        duration: 3000, verticalPosition: 'top',
        horizontalPosition: 'end',
      });
    }
    this.dataSource.paginator = this.paginator;
  }

  openDialog(UserInfo) {
    this.userInfo = new User();
    this.userInfo.name = UserInfo.name;
    this.userInfo.email = UserInfo.email;
    this.userInfo.phoneno = UserInfo.phoneno;
    this.userInfo.address = UserInfo.address;
    this.userInfo.id=UserInfo.id;
    const dialogRef=this.dialog.open(UserUpdateComponent, {
      data: this.userInfo,
    });

    dialogRef.afterClosed().subscribe(result=>{
      if(result="ok"){
        this.ngOnInit();
      }
    });
  }
  

  deleteUserInfo(userinfoId) {
    this.userService.delete(userinfoId).subscribe(result => this.gotoUserList());
  }

  gotoUserList() {
    this.router.navigate(['/users']);
    this.ngOnInit();
    this._snackBar.open("Deleted successfuly", "", {
      duration: 3000, verticalPosition: 'top',
      horizontalPosition: 'end',
    });
  }


}


