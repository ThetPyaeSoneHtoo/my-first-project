import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User } from '../model/user';
import { Observable } from 'rxjs';

@Injectable()
export class UserService {

  private usersUrl: string;

  constructor(private http: HttpClient) {
    // this.usersUrl = 'http://localhost:8080/users';
  }

  public findAll(){
    return this.http.get<User[]>(this.usersUrl='http://localhost:8080/users').toPromise();
  }

  public save(user: User) {
    return this.http.post<User>(this.usersUrl='http://localhost:8080/users', user);
  }

  public update(user: User) {
    return this.http.post<User>(this.usersUrl='http://localhost:8080/updateuser',user);
  }

  public delete(id: string) {
    return this.http.post<string>(this.usersUrl='http://localhost:8080/deleteuser',id);
  }

}
